import { createApp } from "vue";
import { plugin, defaultConfig } from "@formkit/vue";
import { de, fr, zh, en } from '@formkit/i18n';
import '@formkit/themes/genesis';
import { createI18n } from "vue-i18n";
import App from "./App.vue";

import messages from "@intlify/unplugin-vue-i18n/messages";

const i18n = createI18n({
    legacy: false,
    globalInjection: true,
    locale: "en",
    fallbackLocale: "en",
    availableLocales: ["en", "fr"],
    messages: messages,
  });

const app = createApp(App)
app.use(
  plugin,
  defaultConfig({
    // Define additional locales
    locales: { de, fr, zh, en },
    // Define the active locale
    locale: 'en',
  })
)
app.use(i18n)
app.mount('#app')